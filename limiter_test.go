package limiter

import (
	"bytes"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"gitea.com/lunny/tango"
	"github.com/stretchr/testify/assert"
)

func TestLimiter(t *testing.T) {
	o := tango.Classic()
	o.Get("/", func(ctx *tango.Context) {
		fmt.Println("----")
	}, RequestLimit(New(100, 1, 5*time.Second)))

	for i := 0; i < 200; i++ {
		t.Run(fmt.Sprintf("%d", i), func(t *testing.T) {
			buff := bytes.NewBufferString("")
			recorder := httptest.NewRecorder()
			recorder.Body = buff

			req, err := http.NewRequest("GET", "/", nil)
			if err != nil {
				t.Error(err)
			}
			o.ServeHTTP(recorder, req)
			assert.EqualValues(t, recorder.Code, http.StatusOK)
		})
	}
}
