module gitea.com/tango/limiter

go 1.20

require (
	gitea.com/lunny/tango v0.6.2
	github.com/stretchr/testify v1.6.1
	golang.org/x/net v0.0.0-20200625001655-4c5254603344
	golang.org/x/time v0.0.0-20200630173020-3af7569d3a1e
)

require (
	gitea.com/lunny/log v0.0.0-20190322053110-01b5df579c4e // indirect
	github.com/davecgh/go-spew v1.1.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c // indirect
)
