# Limiter

Rate limiter for tango, a global limitation

```go
o.Use(limiter.RequestLimit(limiter.New(100, 1, 5*time.Second))
```

or a limit for a special router

```go
o.Get("/", func(ctx *tango.Context) {}, limiter.RequestLimit(limiter.New(100, 1, 5*time.Second)))
```